<!doctype html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>お問い合わせ｜山梨県自閉症協会</title>
  <link rel="stylesheet" href="../css/ress.css">
  <link rel="stylesheet" href="../css/style.css">

</head>

<body>
  <?php
  require_once('helpers.php');
  if (session_id() === '')
    session_start();

  if (!isset($_SESSION['check_form'])) {
    $_SESSION['check_form'] = false;
  }
  ?>
  <header>
  <div class="box"></div>
    <div id="header-inner">
      <div class="logo">
        <a href="../index.html">
          <img src="../images/logo.png" alt="山梨県自閉症協会 一般社団法人日本自閉症協会団体加盟会員">
        </a>
      </div>
      <nav class="menu">
        <ul class="menu-title">
          <li>
            <a href="../index.html">トップページ</a>
          </li>
          <li>
            <a href="../about.html">⾃閉症協会について</a>
          </li>
          <li>
            <a href="../membership.html">⼊会のご案内</a>
          </li>
          <li>
            <a href="../autism.html">自閉症ってなあに？</a>
          </li>
          <li>
            <a href="../list.html">各種機関窓⼝⼀覧</a>
          </li>
          <li class="menu-a active">
            <a href="pager.php">お問い合わせ</a>
          </li>
        </ul>
      </nav>
      <div class="banner-page">
      <div class="banner">
        <div class="banner-title">
          <p>自閉症などの広汎性発達障害のお子さんをお持ちのお父さん・お母さん、<br>一人で悩んでいないで<br>私たちといっしょにお話ししてみませんか？</p>
        </div>
      </div>
    </div>
  </header>
  <article>
    <div class="container">
      <div class="column-left-list">
        <div class="breadcrumbs">
          <a href="index.html">トップページ</a> >
          <a href="pager.php">お問い合わせ</a>
        </div>
        <h1>お問い合わせ</h1>
        <div class="content-mail">
          <p>山梨県自閉症協会に入会をご希望の方は、入会申込書をお送りいたしますので、必ず郵便番号、住所をご記入ください。入会手続きについて、詳しくは
            <a href="../membership.html">山梨県自閉症協会入会のご案内</a>をご覧ください。</p>
          <form class="cmxform h-adr" id="signupForm" method="POST" action="review.php">
            <dl class="mailform">
              <dt>
                お名前<span class="red">（必須）</span>
              </dt>
              <dd>記入例）　山梨　花子<br>
                <input id="firstname" name="firstname" value="<?= old_value('firstname') ?>" type="text" size="50">
              </dd>
              <dt>
                お名前ふりがな<span class="red">（必須）</span>
              </dt>
              <dd>記入例）　やまなし　はなこ<br>
                <input id="lastname" name="lastname" value="<?= old_value('lastname') ?>" type="text" size="50">
              </dd>
              <dt>
                メールアドレス<span class="red">（必須）</span>
              </dt>
              <dd>記入例）　info@asj-yamanashi.org<br>
                <input id="email" name="email" value="<?= old_value('email') ?>" type="text" size="50">
              </dd>
              <dt>
                郵便番号
              </dt>
              <dd>記入例）　400-0000<br>
                <input type="text" id="zipcode" class="p-postal-code" maxlength="8" name="zipcode" value="<?= old_value('zipcode') ?>">
              </dd>
              <dt>
                ご住所
              </dt>
              <dd>記入例）　山梨県甲府市丸の内0-00-00<br>
                <span class="p-country-name">Japan</span>
                <input id="add" class="p-region p-locality p-street-address p-extended-address" name="add" value="<?= old_value('add') ?>" type="text" size="50">
              </dd>
              <dt>
                性別
              </dt>
              <dd>
                <label for="sex1" class="check">
                  <input type="radio" id="sex1" value="1" name="sex" <?= old_value('sex') == 1 ? 'checked' : '' ?>>男性
                </label>
                <label for="sex2" class="check">
                  <input type="radio" id="sex2" value="2" name="sex" <?= old_value('sex') == 2 ? 'checked' : '' ?>>女性
                </label>
              </dd>
              <dt>
                年代
              </dt>
              <dd>
                <label for="generation1" class="check">
                  <input type="radio" id="generation1" value="1" name="generation" <?= old_value('generation') == '1' ? 'checked' : '' ?>>20歳以下
                </label>
                <label for="generation2" class="check">
                  <input type="radio" id="generation2" value="2" name="generation" <?= old_value('generation') == '2' ? 'checked' : '' ?>>20歳代
                </label>
                <label for="generation3" class="check">
                  <input type="radio" id="generation3" value="3" name="generation" <?= old_value('generation') == 3 ? 'checked' : '' ?>>30歳代
                </label>
                <label for="generation4" class="check">
                  <input type="radio" id="generation4" value="4" name="generation" <?= old_value('generation') == 4 ? 'checked' : '' ?>>40歳代
                </label>
                <label for="generation5" class="check">
                  <input type="radio" id="generation5" value="5" name="generation" <?= old_value('generation') == 5 ? 'checked' : '' ?>>50歳代
                </label>
                <label for="generation6" class="check">
                  <input type="radio" id="generation6" value="6" name="generation" <?= old_value('generation') == 6 ? 'checked' : '' ?>>60歳以上
                </label>
              </dd>
              <dt>
                あなたと自閉症との関係は？
              </dt>
              <dd>
                <label for="relation1" class="check">
                  <input type="radio" id="relation1" value="1" name="relation" <?= old_value('relation') == 1 ? 'checked' : '' ?>>当事者
                </label>
                <label for="relation2" class="check">
                  <input type="radio" id="relation2" value="2" name="relation" <?= old_value('relation') == 2 ? 'checked' : '' ?>>保護者
                </label>
                <label for="relation3" class="check">
                  <input type="radio" id="relation3" value="3" name="relation" <?= old_value('relation') == 3 ? 'checked' : '' ?>>支援者
                </label>
              </dd>
              <dt>
                お問い合わせ項目（必須）
              </dt>
              <dd>
                <label for="item1">
                  <input type="checkbox" id="item1" value="1" name="item[]" <?= in_array(1, old_value('item', [])) ? 'checked' : '' ?>>自閉症協会入会について
                </label><br>
                <label for="item2">
                  <input type="checkbox" id="item2" value="2" name="item[]" <?= in_array(2, old_value('item', [])) ? 'checked' : '' ?>>活動内容にについて
                </label><br>
                <label for="item3">
                  <input type="checkbox" id="item3" value="3" name="item[]" <?= in_array(4, old_value('item', [])) ? 'checked' : '' ?>>その他
                </label>
                <div id="mg-item"></div>
              </dd>
              <dt>
                お問い合わせ内容<span class="red">(必須)</span>
              </dt>
              <dd>
                <textarea cols="45" rows="10" name="note"><?= old_value('note') ?></textarea>
              </dd>
              <dt>
                個人情報の取扱い
              </dt>
              <dd>
                <textarea cols="45" rows="8" readonly="readonly" accesskey="p" tabindex="22">・山梨県自閉症協会は、このお問い合わせフォームでご提供いただきました個人情報を、お問い合わせへの回答の目的のために利用いたします。

・ご提供いただきました個人情報が流出しないようその管理に最大限努力し、本人の許可なく個人情報を第三者に提供することはありません。ただし、法律の適用を受ける場合や法的効力のある要求による場合には、この限りではありません。またそれらの情報を営利目的のために使用することもございません。

・本フォームでご提供頂きましたメールアドレスに誤りがあった場合やシステム障害などの場合にはご回答できない場合があります。

・お問い合わせの内容によっては、電話や書面にて回答させていただく場合があります。</textarea><br>
                <label for="agree">
                  <input type="checkbox" class="checkbox" id="agree" value="1" name="agree" <?= old_value('agree') == 1 ? 'checked' : '' ?>> 上記事項に同意します
                </label>
                <div id="mg-agree"></div>
              </dd>
            </dl>
            <input class="submit" type="submit" value="内容確認ページへ">
            <input class="reset" onclick="location.reload()" type="reset" value="リセット">
          </form>
        </div>
      </div>
    </div>
    <?php unset($_SESSION['form_data']); ?>
  </article>
  <footer>
    <div class="footerinfo">
      <p>Copyright © 2009 ⼭梨県⾃閉症協会 All Rights Reserved.</p>
    </div>
  </footer>

  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery.validate.min.js"></script>
  <script src="../js/yubinbango.js" charset="UTF-8"></script>
  <script src="../js/mail.js"></script>

</body>

</html>