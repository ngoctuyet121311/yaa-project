<!doctype html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>山梨県自閉症協会 - お知らせ</title>
  <link rel="stylesheet" href="../css/ress.css">
  <link rel="stylesheet" href="../css/style.css">
</head>

<body>
  <?php
  require_once('helpers.php');
  if (session_id() === '')
    session_start();

  if (isset($_SESSION['check_form']) && $_SERVER["REQUEST_METHOD"] == "POST") {
    $_SESSION['check_form'] = true;
    // Get data from contact form
    $data = $_POST; //lấy dữ liệu từ pager.php
    $errors = validate($data); //kiểm tra lỗi

    $data = filter($data, $errors); //lọc dữ liệu hợp lệ
    foreach ($data as $key => $value) {
      $_SESSION['form_data'][$key] = $value;
    }
  } else {
    header('Location: pager.php');
    exit();
  }
  ?>
  <header>
    <div id="header-inner">
      <div class="logo">
        <a href="../index.html">
          <img src="../images/logo.png" alt="山梨県自閉症協会 一般社団法人日本自閉症協会団体加盟会員">
        </a>
      </div>
      <nav class="menu">
        <ul class="menu-title"> 
          <li>
            <a href="../index.html">トップページ</a>
          </li>
          <li>
            <a href="../about.html">⾃閉症協会について</a>
          </li>
          <li>
            <a href="../membership.html">⼊会のご案内</a>
          </li>
          <li>
            <a href="../autism.html">⾃閉症ってなあに︖</a>
          </li>
          <li>
            <a href="../list.html">各種機関窓⼝⼀覧</a>
          </li>
          <li class="menu-a active">
            <a href="pager.php">お問い合わせ</a>
          </li>
        </ul>
      </nav>
      <div class="banner-page">
      <div class="banner">
        <div class="banner-title">
          <p>自閉症などの広汎性発達障害のお子さんをお持ちのお父さん・お母さん、<br>一人で悩んでいないで<br>私たちといっしょにお話ししてみませんか？</p>
        </div>
      </div>
      </div>
    </div>
  </header>
  <article>
    <div class="container">
      <div class="column-left-list">
        <h1>山梨県自閉症協会　お問い合わせの確認</h1>
        <div class="alert" id="alert">
          <p>お問い合わせいただきありがとうございました。<br>ページが自動的に更新されない場合は <a href="../index.html">ここ</a>をクリックしてください</p>
        </div>
        <!-- <p class="mailerror">お名前（必須）が入力されていません。<br>お名前ふりがな（必須）が入力されていません。<br> メールアドレス（必須）が入力されていません。<br>お問い合わせ内容（必須）が入力されていません。<br>個人情報の取扱いが入力されていません。</p> -->
        <?php if (count($errors)) : ?>
          <p class="error-text">入力に不備があります！</p>
          <p class="mailerror">
            <?php
            foreach ($errors as $value) { // lọc biến lỗi để in ra
              echo $value . '<br/>';
            }
            ?>
          </p>
        <?php endif; ?>

        <p class="mailresult">
          <?php if (isset($data['firstname'])) : ?>
            お名前（必須）：<?= $data['firstname'] ?> <br />
          <?php endif; ?>

          <?php if (isset($data['lastname'])) : ?>
            お名前ふりがな（必須）: <?= $data['lastname'] ?> <br />
          <?php endif; ?>

          <?php if (isset($data['email'])) : ?>
            メールアドレス（必須：<?= $data['email'] ?> <br />
          <?php endif; ?>

          <?php if (isset($data['zipcode'])) : ?>
            郵便番号：<?= $data['zipcode'] ?> <br />
          <?php endif; ?>

          <?php if (isset($data['add'])) : ?>
            ご住所：<?= $data['add'] ?> <br />
          <?php endif; ?>

          <?php if (isset($data['sex'])) : ?>
            性別：
            <?php
            switch ($data['sex']) {
              case '1':
                echo "男性";
                break;
              case '2':
                echo "女性";
                break;
              default:
                break;
            }
            ?>
            <br />
          <?php endif; ?>
          <?php if (isset($data['generation'])) : ?>
            年代：
            <?php
            switch ($data['generation']) {
              case '1':
                echo "20歳以下";
                break;
              case '2':
                echo "20歳代";
                break;
              case '3':
                echo "30歳代";
                break;
              case '4':
                echo "40歳代";
                break;
              case '5':
                echo "50歳代";
                break;
              case '6':
                echo "60歳以上";
                break;
              default:
                break;
            }
            ?>
            <br />
          <?php endif; ?>

          <?php if (isset($data['relation'])) : ?>
            あなたと自閉症との関係は？:
            <?php
            switch ($data['relation']) {
              case '1':
                echo "当事者";
                break;
              case '2':
                echo "保護者";
                break;
              case '3':
                echo "支援者";
                break;
              default:
                break;
            }
            ?>
            <br />
          <?php endif; ?>

          <?php if (isset($data['item'])) : ?>
            お問い合わせ項目:
            <?php
            foreach ($data['item'] as $item) {
              switch ($item) {
                case '1':
                  echo "自閉症協会入会について. ";
                  break;
                case '2':
                  echo "活動内容にについて. ";
                  break;
                case '3':
                  echo "その他.";
                  break;
                default:
                  break;
              }
            }
            ?>
            <br />
          <?php endif; ?>

          <?php if (isset($data['note'])) : ?>
            お問い合わせ内容(必須): <?= $data['note'] ?> <br />
          <?php endif; ?>

          <?php if (isset($data['agree']) && $data['agree'] == 1) : ?>
            個人情報の取扱い：個人情報の取り扱い事項に同意する<br />
          <?php endif; ?>
        </p><br>
        <form name="form1" method="post" action="send_email.php" class="note">
          <input type="hidden" name="email" value="<?= isset($data['email']) ? $data['email'] : "" ?>" />
          <?php if (count($errors)) : ?>
            <p class="error-text">入力に不備あります。戻るボタンで前の画面へお戻りください。</p>
          <?php endif; ?>

          <?php if (count($errors) == 0) : ?>
            <p style="font-size: 1.4rem; margin:2.3rem 0">この内容でよろしければ送信ボタンを押して送信ください。<br>
              やり直す場合は、戻るボタンで前の画面へお戻りください。</p><br>
            <label for="replies" class="replies">
              <input type="checkbox" class="checkbox" id="replies" value="1" name="replies"> 自動返信を受取らない
            </label><br>
            <input type="button" id="send_mail" value="送信">
          <?php endif; ?>
          <input type="button" id="back-to-form" value="戻る">
        </form>
      </div>
    </div>
    <?php unset($_SESSION['check_form']); ?>
  </article>

  <section class="overloading" id="loading">
    <div class="loading-content">
      <span class="loader loader-double"></span>
      しばらくお待ちください...
    </div>
  </section>

  <footer>
    <div class="footerinfo">
      <p>Copyright © 2009 ⼭梨県⾃閉症協会 All Rights Reserved.</p>
    </div>
  </footer>

  <script src="../js/jquery.min.js"></script>
  <script src="../js/mail.js"></script>

</body>

</html>