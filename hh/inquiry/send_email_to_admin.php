<?php

use PHPMailer\PHPMailer\PHPMailer;
//Thêm các thư viện của Mailer
require_once "PHPMailer/PHPMailer.php";
require_once "PHPMailer/SMTP.php";
require_once "PHPMailer/Exception.php";
require_once 'mail_config.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // Set PHPMailer
  $mail = new PHPMailer();
  // SMTP Settings
  $mail->isSMTP();
  $mail->CharSet = CHART_SET;
  $mail->Host = MAIL_HOST;
  $mail->SMTPAuth = true;
  $mail->Username = MAIL_USERNAME;
  $mail->Password = MAIL_PASSWORD;
  $mail->Port = MAIL_PORT;
  $mail->SMTPSecure = MAIL_ENCRYPTION;

  // Email Settings
  $mail->isHTML(true);
  $mail->setFrom('lara2020webmaster@gmail.com', '山梨県自閉症協会'); // Người gửi
  $mail->addAddress('lara2020webmaster@gmail.com', '管理者'); // Email nhận
  $mail->Subject = '山梨県自閉症協会 '.'   '.'お問い合わせ受付のお知らせ'; // Title mail
  
  if (session_id() === '')
    session_start();
    $data = $_SESSION['form_data'];

  $sex = $data['sex'];
  $sex_str = $tmp = "";
    switch ($sex) {
      case '1':
        $tmp = "男性";
        break;
      case '2':
        $tmp = "女性";
        break;
      default: 
        $tmp = "";
        break;
    }
    $sex_str .= $tmp;


  $generation = $data['generation'];
  $generation_str = $tmp = "";
    switch ($generation) {
      case '1':
        $tmp = "20歳以下";
        break;
      case '2':
        $tmp = "20歳代";
        break;
      case '3':
        $tmp = "30歳代";
        break;
      case '3':
        $tmp = "40歳代";
        break;
      case '3':
        $tmp = "50歳代";
        break;
      case '3':
        $tmp = "60歳以上";
        break;
    default: 
        $tmp = "";
        break;
    }
    $generation_str .= $tmp;


  $relation = $data['relation'];
  $relation_str = $tmp = "";
    switch ($relation) {
      case '1':
        $tmp = "当事者";
        break;
      case '2':
        $tmp = "保護者";
        break;
      case '3':
        $tmp = "支援者";
        break;
      default: 
        $tmp = "";
        break;
    }
    $relation_str .= $tmp;

  $items = $data['item'];
  $item_str = $tmp = "";
  foreach($items as $item){
    switch ($item) {
      case '1':
        $tmp = "自閉症協会入会について. ";
        break;
      case '2':
        $tmp = "活動内容にについて. ";
        break;
      case '3':
        $tmp = "その他.";
        break;
      default: 
        $tmp = "";
        break;
    }
    $item_str .= $tmp;
  }

  $agree = $data['agree'] == 1 ? "個人情報の取り扱い事項に同意する":"";

  $mail->Body = 'ホームページのメールフォームより、以下のお問い合わせを受け付けました。<br>'.
  '<br> お名前:'.$data['firstname'].
  '<br> お名前ふりがな:'.$data['lastname'].
  '<br> メールアドレス:' .$data['email']. 
  '<br> 郵便番号: '.$data['zipcode'].
  '<br> ご住所: '.$data['add'].
  '<br> 性別:'. $sex_str.
  '<br> 年代:'. $generation_str.
  '<br> あなたと自閉症との関係は？:'. $relation_str.
  '<br> お問合せ項目: '. $item_str.
  '<br> ご住所: '.$data['note'].
  '<br> ご住所: '.$agree.'<br>';

  if ($mail->send()) {
    $status = 'success';
    $message = "Sent mail successfully";
  } else {
    $status  = 'mail_error';
    $message = "Something is wrong: " . $mail->ErrorInfo;
  }

  exit(json_encode(array("status" => $status, "message" => $message)));
}