<?php

/**
 * Validate mail form
 * 
 * @param array $data
 * @return array $errors
 * 
 */
function validate($data)
{
  $errors = [];

  if (empty($data["firstname"])) {
    $errors['firstname'] = "お名前が入力されていません。";
  }

  if (empty($data["lastname"])) {
    $errors['lastname'] = "お名前ふりがなが入力されていません";
  }

  if (empty($data["email"])) {
    $errors['email'] = "メールアドレスが入力されていません。";
  } else {
    // check e-mail address is valid
    if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
      $errors['email'] = "エラーメール";
    }
  }

  // $zipcode = trim($data["zipcode"]);
  // if (strlen($zipcode)!==7) {
  //     $errors['zipcode'] = "must be 7";
  // }

  // if (empty($data["add"])) {
  //     $errors['add'] = "未入力チェック";
  // }

  if (!isset($data["item"]) || count($data['item']) == 0) {
    $errors['item'] = "お問い合わせ項目が入力されていません。";
  }

  if (empty($data["note"])) {
    $errors['note'] = "お問い合わせ内容が入力されていません。";
  }

  if (empty($data["agree"])) {
    $errors['agree'] = "個人情報の取扱いには同意が必要です。";
  }
  return $errors;
}

// Filter valid data from the form lọc dữ liệu hợp lệ
function filter($data, $errors)
{
  foreach ($data as $key => $value) {
    if (isset($errors[$key])) {
      unset($data[$key]);
    }
  }
  return $data;
}

// Get old value lấy lại giá trị cũ
function old_value($name, $default = "")
{
  if (isset($_SESSION['form_data'][$name])) {
    return $_SESSION['form_data'][$name];
  }
  return $default;
}
