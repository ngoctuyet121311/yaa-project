//----------------error-----------------
$(document).ready(function() {
    var form = $('#signupForm');
    form.validate({
      rules: {
        email: {
          required: true,
          email: true,
          // regex: /^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i,
        },
        firstname: {
          required: true,
        },
        lastname: {
          required: true,
        },
        zipcode: {
          required: true,
          minlength: 8,
          maxlength: 8,
          // digits: true //kiểm tra kí tự
        },
        "item[]": {
          required: true,
        },
        note: {
          required: true,
        },
        agree: {
          required: true,
        }
      },
      messages: {
        email: {
          required: '未入力',
          email: '形式（メールアドレス）',
          // regex: '形式（メールアドレス）',
        },
        firstname: {
          required: '未入力',
        },
        lastname: {
          required: '未入力',
        },
        zipcode: {
          required: '形式（数値）',
          maxlength: '形式（文字数）',
          minlength: '形式（文字数）',
          digits: '形式（数値）',
        },
        "item[]": {
          required: '未入力',
        },
        note: {
          required: '未入力',
        },
        agree: {
          required: '未入力',
        }
      },
      onsubmit: false,
      onfocusout: function(element) {
        this.element(element)
      },
      errorPlacement: function(error, element) {
        if (element.attr("name") == "item[]") {
          error.insertAfter("#mg-item");
        } else if (element.attr("name") == "agree") {
          error.insertAfter("#mg-agree");
        } else {
          error.insertAfter(element);
        }
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  });


  // Overloading
  var $loading = $('#loading').hide();
  $(document)
    .ajaxStart(function() {
      $loading.show();
    })
    .ajaxStop(function() {
      $loading.hide();
    });

  $(document).ready(function() {

    $("#back-to-form").click(function() {
      window.location.replace("pager.php");
    });

    $("#send_mail").click(function() {
      $.ajax({
        url: 'send_email.php',
        method: 'POST',
        dataType: 'json',
        data: {
          email: $("input[name=email]").val()
        },
        success: function(response) {
          if (response.status == 'success'){
            $("#alert").show();  
            $(".note").hide();
            $(".mailresult").hide();
            setTimeout(function() {
              location.replace("../index.html");
            }, 3000);
            console.log(response);
          } 
          else {
            // console.log(response);
            location.replace("pager.php");
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError);
        }
      });

      $.ajax({
        url: 'send_email_to_admin.php',
        method: 'POST',
        dataType: 'json',
        data: {
          email: $("input[name=email]").val()
        },
        success: function(response) {
          if (response.status == 'success'){
            $("#alert").show();  
            $(".note").hide();
            $(".mailresult").hide();
            setTimeout(function() {
              location.replace("../index.html");
            }, 3000);
            console.log(response);
          } 
          else {
            // console.log(response);
            location.replace("pager.php");
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError);
        }
      });
    })
  });